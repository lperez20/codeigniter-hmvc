<!DOCTYPE html>
<html>
    <head>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link rel="icon" href="../../favicon.ico"> -->

        <title>Navbar Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="https://v4-alpha.getbootstrap.com/examples/navbars/navbar.css" rel="stylesheet">
      </head>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header text-center">Formulario</h1>
                    <hr>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-4">
                </div>

                <div class="col-lg-4">
                    <h3>Datos</h3>
                    <form action="inicio/registrar_datos" method="post" name="datos_usuario">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input class="form-control" type="text" id="nombre" name="nombre">
                        </div>

                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input class="form-control" type="text" id="apellido" name="apellido">
                        </div>

                        <button class="btn btn-sm btn-block btn-success" type="submit">Enviar</button>
                    </form>
                </div>

                <div class="col-lg-4">
                </div>
            </div>
        </div>
    </body>
</html>